console.log("========Jawaban No 1========")

let panjang= 12
let lebar= 4
let tinggi = 8

const HasilluasPersegiPanjang =(panjang,lebar)=>{
    return panjang*lebar
}

const HasilkelilingPersegiPanjang =(panjang,lebar)=>{
    return panjang*lebar
}

const HasilvolumeBalok =(panjang,lebar,tinggi)=>{
    return panjang*lebar*tinggi
}

console.log(HasilkelilingPersegiPanjang (panjang,lebar))
console.log(HasilluasPersegiPanjang (panjang,lebar))
console.log(HasilvolumeBalok(panjang,lebar,tinggi))


//=======================================================
console.log("========Jawaban No 2========")

function introduce(...someArgs){
    let [nama,umur,pekerjaan]=someArgs
    return` Pak ${nama} adalah seorang ${pekerjaan} yang berusia ${umur} tahun`

} 

//kode di bawah ini jangan dirubah atau dihapus
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

console.log("========Jawaban No 3 ========")


let arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , 1992]
let objDaftarPeserta = {
    nama:"John Doe",
    jenis_kelamin:"laki-laki",
    hobi:"baca buku",
    tahun_lahir:1992
}
console.log(objDaftarPeserta)



//=======================================================
console.log("========Jawaban No 4========")
var buah=[
    {nama:"Nanas",warna:"kuning",ada_bijinya:"tidak",harga:9000},
    {nama:"jeruk",warna:"orange",ada_bijinya:"ada",harga:8000},
    {nama:"semangka",warna:"hijau & merah",ada_bijinya:"ada",harga:10000},
    {nama:"pisang",warna:"kuning",ada_bijinya:"tidak",harga:5000}
    
];

var cetakObjectBuah=buah.filter(function(buahs){
    return buahs.ada_bijinya="tidak";
});

console.log(cetakObjectBuah);

//=======================================================
console.log("========Jawaban No 5 ========")

let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020,
    colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
 }
 // kode diatas ini jangan di rubah atau di hapus sama sekali
 
 /* Tulis kode jawabannya di sini */
 // kode di bawah ini jangan dirubah atau dihapus
 console.log(phoneBrand, phoneName, year, colorBlack, colorBronze) 