 console.log("=======Jawaban nomer 1======")
let dataPeserta = ["john", "laki-laki", "programmer", "30"]
const [nama,jenis_kelamin,pekerjaan,umur]=dataPeserta

let output=`Halo, nama saya ${nama}. saya ${jenis_kelamin}  berumur ${umur} bekerja sebagai seorang ${pekerjaan} `;

console.log(output)


console.log("=======Jawaban nomer 2 ======")

let array1 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"]
for (var i=0;i<array1.length;i++) {
    console.log(array1[i])
}

console.log("=======Jawaban nomer 3 ======")

let array2 = [1, 2, 3, 4, 5, 6,7, 8, 9, 10]

for (var i=1;i<=array2.length;i++){
    if(array2[i]%2===0){
        
        console.log("Angka Genap",i+1)
    }
}

console.log("=======Jawaban nomer 4 ======")

let kalimat= ["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"]

var arr2=kalimat.slice(1,7)
console.log(arr2)
//==========pecahan ===========
arr2.splice(1,1)
console.log(arr2)


console.log("=======Jawaban nomer 5 ======")

var sayuran=[]
sayuran.push('Kangkung','Bayam','Buncis','Kubis','Timun','Seledri','Tauge')
sayuran.sort()

var cetak=function(e,i){
    console.log(e)
}
sayuran.forEach(function(e,i){
    console.log("Nomer ke-",(i+1),e)
})